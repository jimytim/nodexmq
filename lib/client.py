
import json
import logging
import random
import sys
import time
from struct import pack, unpack

import zmq

class Client:

    def __init__(self, context=None, transport="tcp"):
        self.transport = transport or "tcp"
        self.client_id = None
        self.client_name = None
        self.state = {"client":{}}
        self.init_data = None
        self.poll_timeout = 0
        self.sequence = -1

        # Prepare our context and client sockets
        if context:
            self.ctx = context 
        elif self.transport in ["tcp", "ipc"]:
            self.ctx = zmq.Context()
        else: # Multithreading
            assert transport == "inproc"
            self.ctx = zmq.Context.instance()
        
        requester_address, publisher_address, subscriber_address = self.configure_addresses(transport)
        
        # self.requester.setsockopt(zmq.IDENTITY, b"CLIENT")
        self.requester = self.ctx.socket(zmq.DEALER)
        self.publisher = self.ctx.socket(zmq.PUSH)
        self.subscriber = self.ctx.socket(zmq.SUB)
        self.subscriber.setsockopt(zmq.SUBSCRIBE, "/client".encode())
        self.subscriber.setsockopt(zmq.SUBSCRIBE, "/task".encode())
        
        self.requester.linger = 0
        self.publisher.linger = 0
        self.subscriber.linger = 0

        self.requester.connect(requester_address)
        self.subscriber.connect(publisher_address)
        self.publisher.connect(subscriber_address)

        self.poller = zmq.Poller()
        self.poller.register(self.subscriber, zmq.POLLIN)

        self.logger = None

        self.send_greetings()
        self.request_state()
    

    def set_logger(self, name):
        self.logger_name = name
        self.logger = logging.getLogger(name)
        self.logger.setLevel(logging.DEBUG)

        # Console formatting
        consoleHandler = logging.StreamHandler(stream=sys.stdout)
        consoleHandler.setLevel(logging.DEBUG)
        format_string = "{{name:<{length}}} - {{levelname}} - {{message}}".format(length=len(self.logger_name))
        consoleFormatter = logging.Formatter(format_string, style='{')
        consoleHandler.setFormatter(consoleFormatter)
        self.logger.addHandler(consoleHandler)


    def configure_addresses(self, transport, port=5556):
        requester_address = None
        publisher_address = None
        subscriber_address = None

        if transport == "tcp":
            requester_address = f"tcp://localhost:{port}"
            publisher_address = f"tcp://localhost:{port+1}"
            subscriber_address = f"tcp://localhost:{port+2}"

        elif transport == "ipc":
            requester_address = "ipc://server.ipc"
            publisher_address = "ipc://state.ipc"
            subscriber_address = "ipc://update.ipc"

        elif transport == "inproc":
            requester_address = "inproc://server"
            publisher_address = "inproc://state"
            subscriber_address = "inproc://update"

        return requester_address, publisher_address, subscriber_address


    def send_greetings(self):
        # Send greetings
        # self.logger.debug("Sending greetings")
        print("Client: Sending greetings...")
        self.requester.send_multipart([b"greeting", b""])

        # Waiting for a welcome
        while True:
            
            try:
                key, client_name_b, client_data_b = self.requester.recv_multipart()
            except KeyboardInterrupt:
                print("Shutdown requested... exiting")
                sys.exit(0)
            except:
                raise
            
            if key == b"Welcome":
                self.client_name = client_name_b.decode()
                self.client_id = self.client_name.split('_')[1]
                self.set_logger(self.client_name)
                self.init_data = client_data_b
                self.state["client"][self.client_name] = {}
                self.logger.debug(f"Received a welcome, I am client '{self.client_name}'")
                break


    def request_state(self):
        # Ask for a state
        self.logger.debug("Asking for the state")
        self.requester.send_multipart([b"state", self.client_name.encode()])

        # Waiting for each state entrie
        while True:
            try:
                key, data = self.requester.recv_multipart()
            except KeyboardInterrupt:
                print("Shutdown requested... exiting")
                sys.exit(0)
            except:
                raise

            if key != b"end":
                self.state["client"][key.decode()] = json.loads(data)

            else:
                # We sync with the server update number
                self.sequence = unpack("!q", data)[0]
                self.logger.debug(f"Received state (State up to update seq #{self.sequence})")
                break  # Done
                

    def request_task(self):
        # Ask for a tasks
        self.logger.debug("Asking for a task")
        self.requester.send_multipart([b"task", self.client_name.encode()])
        while True:
            try:
                key, *task_data = self.requester.recv_multipart()
            except KeyboardInterrupt:
                print("Shutdown requested... exiting")
                sys.exit(0)
            except:
                raise

            if key == b"Task":
                self.logger.debug("Received a task")
                return task_data
            elif key  == b"NoTask":
                return None


    def send_updates(self, kv):
        update = [self.client_name.encode()]
        for key, value, encoding in kv:
            if isinstance(value, str):
                update.extend([key.encode(), value.encode(), encoding.encode()])
            elif isinstance(value, list) or isinstance(value, tuple):
                update.extend([key.encode(), pack(encoding, *value), encoding.encode()])
            else:
                self.logger.error("Update Sender: Unknow value type")
        self.publisher.send_multipart(update)


    def send_updates_auto(self, kv):
        update = [self.client_name.encode()]
        for key, value in kv:
            if isinstance(value, bytes):
                update.extend([key.encode(), value, b's'])
            elif isinstance(value, str):
                update.extend([key.encode(), value.encode(), b's'])
            elif isinstance(value, list) or isinstance(value, tuple):
                size = len(value)
                # We assume that all items have the same type here
                if isinstance(value[0], float):
                    encoding = f"!{size}d"
                elif isinstance(value[0], int):
                    encoding = f"!{size}q"
                else:
                    self.logger.error("Update Sender: Unknow iterable value type, not numerical")
                update.extend([key.encode(), pack(encoding, *value), encoding.encode()])
            else:
                self.logger.error("Update Sender: Unknow value type")
            
        self.publisher.send_multipart(update)
        

    def update_state(self, subtree, client_name, kv_list):
        logging_str_list = []
        if client_name != self.client_name:

            if client_name not in self.state["client"]:
                self.logger.info(f"{client_name} is unknown, adding it to the map")
                self.state["client"][client_name] = {}

            for i in range(len(kv_list)//3):
                key_b, value_b, encoding = kv_list[3*i], kv_list[3*i+1], kv_list[3*i+2]
                key = key_b.decode()
                value = value_b.decode() if encoding == b's' else unpack(encoding, value_b)
                self.state[subtree][client_name][key] = value
                logging_str_list.append(f"{key}: {value}")

            self.logger.info(f"Server state update #{self.sequence}: {client_name}: {', '.join(logging_str_list)}")


    def pull_state_updates(self):

        try:
            items = dict(self.poller.poll(self.poll_timeout))
        except:
            return           # Interrupted

        if self.subscriber in items:
            # TODO: Pulling multiple updates at a time
            msg = self.subscriber.recv_multipart()
            subtree, msg_sequence, *rest = msg
            msg_sequence_value = unpack("!q", msg_sequence)[0]

            if msg_sequence_value > self.sequence:
                self.sequence = msg_sequence_value
                state_subtree = subtree[1:-1].decode() # We need to trim the subtree b"/client/"
                client_id_b, *kv_list = rest
                self.update_state(state_subtree, client_id_b.decode(), kv_list)
                                
            else:
                self.logger.debug(f"Discarding message #{msg_sequence_value}")

    def send_exit_signal(self):
        self.logger.debug("Sending exit signal")
        self.requester.send_multipart([b"shutdown", self.client_name.encode()])
        self.logger.debug("Stopped.")       

