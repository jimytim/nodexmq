
import random
from itertools import product

def generate_tasks(n_rows, n_cols, n_tasks, random_seed=42):
    """Basic task positions generator
       If n_tasks > n_rows * n_cols:
        we exhaust all possible positions evenly first and restart 
        as long as needed
    
        Returns: 
          A list of tasks [('a',(1,1)), ('b',(1,0)), ('c',(0,0))]
    """
    random.seed(random_seed) 
    
    n_cells = n_rows * n_cols
    tasks_label_list = []
    tasks_coordinate_list = []
    
    if n_tasks > 26:
        tasks_label_list = [f"Task_{n}" for n in range(n_tasks)]
    else:
        from string import ascii_lowercase
        tasks_label_list = list(ascii_lowercase[:n_tasks])
        
    positions = list(product(range(n_rows), range(n_cols)))

    if n_tasks > n_cells:
        task_count = 0
        while task_count < n_tasks - n_cells:
            print(tasks_coordinate_list)
            tasks_coordinate_list.extend(random.sample(positions, n_cells))
            task_count += n_cells
        tasks_coordinate_list.extend(random.sample(positions, n_tasks - task_count))
        
    else:
        tasks_coordinate_list = random.sample(positions, n_tasks)
    
    tasks_list = list(zip(tasks_label_list, tasks_coordinate_list))
#     tasks_dict = {task_label:task_pos for task_label, task_pos in tasks_list}
    return tasks_list