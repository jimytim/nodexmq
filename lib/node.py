import sys
from struct import pack, unpack
from nodexmq.lib.client import Client

class Node:

    def __init__(self, setup):
        self._tracked_attributes = setup.tracked_variables
        self.setup = setup
        self._client = Client(transport="ipc")
        self._attributes_to_update = []
        self._alive = True


    def __setattr__(self, name, value):
        # print(f"name: {name}, value: {value}")
        if name == "_tracked_attributes":
            super(Node, self).__setattr__(name, value)
        else:
            super(Node, self).__setattr__(name, value)
            if name in self._tracked_attributes:
                # print(f"Node: Tracked value: {name} = {value}")
                self._client.state["client"][self._client.client_name][name] = value
                self._attributes_to_update.append(name)


    def initialize(self):
        # TODO: Hardcoded state initialization protocol
        init_data_key = "position"
        init_data_encoding = self.setup.encoding[init_data_key]
        data = unpack(init_data_encoding, self._client.init_data)
        return data


    def get_id(self):
        return self._client.client_id


    def get_input(self):
        input_data = self._client.request_task()
        if input_data:
            # TODO: Hardcoded input data protocol
            task_b, task_pos_b = input_data
            task = task_b.decode()
            task_position_encoding = self.setup.encoding["task_position"]
            t_x, t_y = unpack(task_position_encoding, task_pos_b)
            return task, (t_x, t_y)
        else:
            return None


    def run(self):
        pass


    def execute(self):
        self._push_updates()
        while self._alive:
            try:
                self.run()
                self._push_updates()
                self._pull_updates()
            except KeyboardInterrupt:
                print("Shutdown requested... exiting")
                sys.exit(0)
            except:
                raise


    def _pull_updates(self):
        self._client.pull_state_updates()


    def _push_updates(self):
        if self._attributes_to_update:
            attribute_value_list = []
            for attr_name in self._attributes_to_update:       
                attribute_value_list.extend([(attr_name, getattr(self, attr_name), self.setup.encoding[attr_name])])
            self._client.send_updates(attribute_value_list)
            self._attributes_to_update = []

    
    def stop(self):
        self._client.send_exit_signal()
        self._alive = False

