import configparser
import json
import logging
import sys
import time
from struct import pack, unpack

import zmq
from tornado.ioloop import IOLoop, PeriodicCallback
from zmq.eventloop.zmqstream import ZMQStream

from pathlib import Path
from binascii import hexlify

from helpers import generate_tasks

def dump(msg_or_socket):
    """Receives all message parts from socket, printing each frame neatly"""
    if isinstance(msg_or_socket, zmq.Socket):
        # it's a socket, call on current message
        msg = msg_or_socket.recv_multipart()
    else:
        msg = msg_or_socket
    print("----------------------------------------")
    for part in msg:
        print("[%03d]" % len(part), end=' ')
        is_text = True
        try:
            print(part.decode('ascii'))
        except UnicodeDecodeError:
            print(r"0x%s" % (hexlify(part).decode('ascii')))


class Server:
   # Our server is defined by these properties
    context = None              # Context wrapper
    state = None                # Key-value store
    loop = None                 # IOLoop reactor
    port = None                 # Main port we're working on
    sequence = 0                # How many updates we're at
    responder = None            # Handle clients requests
    publisher = None            # Publish updates to clients
    collector = None            # Collect updates from clients

    def __init__(self, port=5556, config_path="../Config/config.txt", transport="tcp"):
        self.port = port
        self.loop = IOLoop.instance()
        self.state = {"client":{}}
        self.client_counter = 0
        self.last_client_id = 0
        self.config = configparser.ConfigParser()
        self.config.read(config_path)

        # Simulation configuration
        self.n_rows = int(self.config["World Simulation Parameters"]["NbRows"])
        self.n_cols = int(self.config["World Simulation Parameters"]["NbColumns"])
        self.n_tasks = int(self.config["World Simulation Parameters"]["NbTasks"])
        self.random_seed = int(self.config["World Simulation Parameters"]["RandomSeed"])
        self.tasks = generate_tasks(self.n_rows, self.n_cols, self.n_tasks, 
                                    random_seed=self.random_seed)

        self.tasks_dict = {tl:tp for tl, tp in self.tasks}
        
        # Set up our clone server sockets
        self.context = None
        self.transport = transport
        if transport in ["tcp", "ipc"]:
            self.context = zmq.Context()
        else: # Multithreading
            self.context = zmq.Context.instance()

        self.responder = self.context.socket(zmq.ROUTER)
        self.publisher = self.context.socket(zmq.PUB)
        self.collector = self.context.socket(zmq.PULL)

        responder_address, publisher_address, collector_address = self.configure_addresses(transport, port=self.port)
        self.responder.bind(responder_address)
        self.publisher.bind(publisher_address)
        self.collector.bind(collector_address)

        # Wrap sockets in ZMQStreams for IOLoop handlers
        self.responder = ZMQStream(self.responder)
        self.publisher = ZMQStream(self.publisher)
        self.collector = ZMQStream(self.collector)

        # Register our handlers with reactor
        self.responder.on_recv(self.handle_request)
        self.collector.on_recv(self.handle_collect)

        # Logger creation
        self.logger_name = "Server"
        self.log_file_path = Path(self.config["Server"]["LogFilePath"])
        self.logger = logging.getLogger(self.logger_name)
        self.logger.setLevel(logging.DEBUG)

        # Console formatting
        consoleHandler = logging.StreamHandler(stream=sys.stdout)
        consoleHandler.setLevel(logging.DEBUG)
        format_string = "{{name:<{length}}} - {{levelname}} - {{message}}".format(length=len(self.logger_name))
        consoleFormatter = logging.Formatter(format_string, style='{')
        consoleHandler.setFormatter(consoleFormatter)
        self.logger.addHandler(consoleHandler)

        # File formatting
        if self.log_file_path.exists():
            self.log_file_path.unlink()
        fileHandler = logging.FileHandler(self.log_file_path, mode='a')
        fileHandler.setLevel(logging.DEBUG)
        format_string = "{{asctime}}.{{msecs:.0f}} | {{name:<{length}}} | {{levelname:^9}} | {{message}}".format(length=len(self.logger_name))
        fileFormatter = logging.Formatter(format_string, datefmt='%H:%M:%S', style='{')
        # fileFormatter.default_msec_format = "%s.%0f"
        fileHandler.setFormatter(fileFormatter)
        self.logger.addHandler(fileHandler)

        # Records
        self.records_file_path = Path(Path(self.config["Server"]["RecordFilePath"]))
        self.records_file_path.write_text("Client,Task,GridX,GridY\n")

    def configure_addresses(self, transport, port=5556):
        responder_address = None
        publisher_address = None
        collector_address = None

        if transport == "tcp":
            responder_address = f"tcp://*:{port}"
            publisher_address = f"tcp://*:{port+1}"
            collector_address = f"tcp://*:{port+2}"

        elif transport == "ipc":
            responder_address = "ipc://server.ipc"
            publisher_address = "ipc://state.ipc"
            collector_address = "ipc://update.ipc"

        elif transport == "inproc":
            responder_address = "inproc://server"
            publisher_address = "inproc://state"
            collector_address = "inproc://update"

        return responder_address, publisher_address, collector_address


    def start(self):
        try:
            self.loop.start()
        except KeyboardInterrupt:
            pass


    def add_record(self, client, task, x, y):
        line = f"{client},{task},{x:.3f},{y:.3f}\n"
        with self.records_file_path.open(mode='a') as f:
            f.write(line)


    def handle_request(self, msg):
        # Greeting requests
        if len(msg) == 3:
            identity, request_type, data = msg

            if request_type == b"greeting":
                # Sending welcome info
                client_name = f"client_{self.last_client_id}"
                spawn_g_x, spawn_g_y = 0.0, -1.0
                client_data_b =  pack('!2d', spawn_g_x, spawn_g_y)
                self.responder.send_multipart([identity, b"Welcome", client_name.encode(), client_data_b])
    
                # Initiatizing state
                self.state["client"][client_name] = {}

                # Logging
                self.add_record(client_name, "None", spawn_g_x, spawn_g_y)
                self.logger.debug(f"Sending a warm 'Welcome' to new client '{client_name}'")
                self.logger.debug(f"'{client_name}' in position ({spawn_g_x},{spawn_g_y})")

                self.last_client_id += 1
                self.client_counter += 1

            elif request_type == b"task":
                # Send task to client
                client = data.decode()
                self.logger.debug(f"{client} asked for a task")

                if self.tasks:
                    task, pos = self.tasks.pop(0)
                    pos_b = pack("!2d", float(pos[0]), float(pos[1]))
                    self.responder.send_multipart([identity, b"Task", task.encode(), pos_b])

                    self.logger.debug(f"Task '{task}' {pos} assigned to {client}")

                else:
                    if client.startswith("waiting"):
                        self.responder.send_multipart([identity, b"Stop", b'', b''])
                        self.client_counter -= 1
                        self.logger.debug(f"Still no tasks. Sending stop signal... ({self.client_counter} clients remaining)")
                        if self.client_counter == 0:
                            self.logger.info("No more clients. Stopping server...")
                            self.loop.stop()
                            return
                    else:
                        self.responder.send_multipart([identity, b"Wait", b'', b''])
                        self.logger.debug(f"No tasks availaible at the moment")
                    

            elif request_type == b"state":
                # Send state to client
                for client_id, data in self.state["client"].items():
                    data_json_b = json.dumps(data, separators=(',', ':')).encode('ascii')
                    self.responder.send_multipart([identity, client_id.encode(), data_json_b])

                self.responder.send_multipart([identity, b"end", pack("!q", self.sequence)])

                self.logger.debug(f"Sent a state shapshot")

            else:
                self.logger.error("E: invalid request type, aborting")
                dump(msg)
                self.loop.stop()
                return

        else:
            self.logger.error("E: incomplete request, aborting")
            dump(msg)
            self.loop.stop()
            return


    def update_state(self, subtree, client_id, kv_list):
        logging_str_list = []
        value = None
        encoding = None
        for i in range(len(kv_list)//3):
            key_b, value_b, encoding_b = kv_list[3*i], kv_list[3*i+1], kv_list[3*i+2]
            key = key_b.decode()
            encoding = encoding_b.decode()
            value = value_b.decode() if encoding == 's' else unpack(encoding, value_b)
            self.state[subtree][client_id][key] = value
            logging_str_list.append(f"{key}:{value}")

            if key == "position":
                task = self.state["client"][client_id].get("task", "None")
                self.add_record(client_id, task, value[0], value[1])

        return ", ".join(logging_str_list)

        
    def handle_collect(self, msg):
        """Collect and redistribute updates from clients"""

        if len(msg) > 1:
            client_id, *kv_list = msg
            update_log = self.update_state("client", client_id.decode(), kv_list)
            self.logger.info(f"Update #{self.sequence}: {client_id.decode()} -> {update_log}")
            update = [b"/client/", pack("!q", self.sequence), client_id] + kv_list
            self.publisher.send_multipart(update)
            self.sequence += 1
        else:
            self.logger.info(f"Client message: {msg[0].decode()}")
        
    def close(self):
        self.responder.close()
        self.publisher.close()
        self.collector.close()
        self.context.term()

        if self.transport == "ipc":
            for ipc_file_path in Path(".").glob("*.ipc"):
                ipc_file_path.unlink()
        
def main():
    server = Server()
    server.start()

if __name__ == '__main__':
    main()
            