
import json
import logging
import random
import sys
import time
from struct import pack, unpack

import zmq

def dist_n1(x0, y0, x1, y1):
    return abs(x1 - x0) + abs(y1 - y0)

class Client:

    def __init__(self, progress_ping=0.5, task_ping=7, context=None, transport="tcp"):
        self.transport = transport or "tcp"
        self.client_id = 0
        self.client_name = None
        self.state = {"client":{}}
        self.progress_ping = progress_ping
        self.task_ping = task_ping

        self.poll_timeout = 0
        self.busy = False
        self.waiting = False
        self.sequence = -1

        # Simulation
        # Grid position (centered) 
        self.g_x: float = 0.0
        self.g_y: float = 0.0

        self.task = None
        self.task_g_x = None
        self.task_g_y = None
        self.task_distance = None

        self.path = None
        self.task_distance_threshold = 0.2

        self.speed = 0.5

        # Prepare our context and client sockets
        if context:
            self.ctx = context 
        elif self.transport in ["tcp", "ipc"]:
            self.ctx = zmq.Context()
        else: # Multithreading
            assert transport == "inproc"
            self.ctx = zmq.Context.instance()
        
        requester_address, publisher_address, subscriber_address = self.configure_addresses(transport)
        
        # self.requester.setsockopt(zmq.IDENTITY, b"CLIENT")
        self.requester = self.ctx.socket(zmq.DEALER)
        self.publisher = self.ctx.socket(zmq.PUSH)
        self.subscriber = self.ctx.socket(zmq.SUB)
        self.subscriber.setsockopt(zmq.SUBSCRIBE, "/client".encode())
        self.subscriber.setsockopt(zmq.SUBSCRIBE, "/task".encode())
        
        self.requester.linger = 0
        self.publisher.linger = 0
        self.subscriber.linger = 0

        self.requester.connect(requester_address)
        self.subscriber.connect(publisher_address)
        self.publisher.connect(subscriber_address)

        self.logger = None
    
    def set_logger(self, name):
        self.logger_name = name
        self.logger = logging.getLogger(name)
        self.logger.setLevel(logging.DEBUG)

        # Console formatting
        consoleHandler = logging.StreamHandler(stream=sys.stdout)
        consoleHandler.setLevel(logging.DEBUG)
        format_string = "{{name:<{length}}} - {{levelname}} - {{message}}".format(length=len(self.logger_name))
        consoleFormatter = logging.Formatter(format_string, style='{')
        consoleHandler.setFormatter(consoleFormatter)
        self.logger.addHandler(consoleHandler)

    def configure_addresses(self, transport, port=5556):
        requester_address = None
        publisher_address = None
        subscriber_address = None

        if transport == "tcp":
            requester_address = f"tcp://localhost:{port}"
            publisher_address = f"tcp://localhost:{port+1}"
            subscriber_address = f"tcp://localhost:{port+2}"

        elif transport == "ipc":
            requester_address = "ipc://server.ipc"
            publisher_address = "ipc://state.ipc"
            subscriber_address = "ipc://update.ipc"

        elif transport == "inproc":
            requester_address = "inproc://server"
            publisher_address = "inproc://state"
            subscriber_address = "inproc://update"

        return requester_address, publisher_address, subscriber_address

    def start(self):
        self.send_greetings()
        self.request_state()
        self.run_loop()
    
    def solve_path(self):
        self.path = [[self.g_x, self.g_y],
                     [self.g_x, self.task_g_y],
                     [self.task_g_x, self.task_g_y]]
        self.current_segment_start_path_idx = 0
        self.current_segment = [[self.g_x, self.g_y],
                                [self.g_x, self.task_g_y]]
        
    def update_position(self):
        speed = 0.01
        new_g_x, new_g_y = None, None
        x0, y0 = self.current_segment[0]
        x1, y1 = self.current_segment[1]
        reach_end_segment = False
        # vertical segment (x cste)
        if x0 == x1:
            # South direction
            if y0 < y1:
                new_g_y = self.g_y + self.speed
                if new_g_y > y1:
                    self.g_y = y1
                    reach_end_segment = True
                else:
                    self.g_y = new_g_y
                    
            else: # North direction
                new_g_y = self.g_y - self.speed
                if new_g_y < y1:
                    self.g_y = y1
                    reach_end_segment = True
                else:
                    self.g_y = new_g_y
        # horizontal segment (y cste)
        elif y0 == y1:
            # East direction
            if x0 < x1:
                # self.g_x = min(self.g_x + speed, x1)
                new_g_x = self.g_x + self.speed
                if new_g_x > x1:
                    self.g_x = x1
                    reach_end_segment = True
                else:
                    self.g_x = new_g_x
                
            else: # West direction
                # self.g_x = max(self.g_x - speed, x1)
                new_g_x = self.g_x - self.speed
                if new_g_x < x1:
                    self.g_x = x1
                    reach_end_segment = True
                else:
                    self.g_x = new_g_x

        if reach_end_segment:
            self.current_segment_start_path_idx += 1
            idx = self.current_segment_start_path_idx
            self.current_segment = [self.path[idx], self.path[idx+1]]


    def send_greetings(self):
        # Send greetings
        # self.logger.debug("Sending greetings")
        print("Client: Sending greetings...")
        self.requester.send_multipart([b"greeting", b""])

        # Waiting for a welcome
        while True:
            
            try:
                key, client_name_b, client_data_b = self.requester.recv_multipart()
            except KeyboardInterrupt:
                print("Shutdown requested... exiting")
                sys.exit(0)
            except:
                raise
            
            if key == b"Welcome":
                self.client_name = client_name_b.decode()
                self.set_logger(self.client_name)
                self.g_x, self.g_y = unpack('!2d',client_data_b)
                self.state["client"][self.client_name] = {}
                self.send_updates((b"position", client_data_b, b"!2d"), pre_encoded=True)
                self.logger.debug(f"Received a welcome, I am client '{self.client_name}'")
                self.logger.debug(f"Spawned in {self.g_x, self.g_y}")
                break

    def request_state(self):
        # Ask for a state
        self.logger.debug("Asking for the state")
        self.requester.send_multipart([b"state", self.client_name.encode()])

        # Waiting for each state entrie
        while True:
            try:
                key, data = self.requester.recv_multipart()
            except KeyboardInterrupt:
                print("Shutdown requested... exiting")
                sys.exit(0)
            except:
                raise

            if key != b"end":
                self.state["client"][key.decode()] = json.loads(data)

            else:
                # We sync with the server update number
                self.sequence = unpack("!q", data)[0]
                self.logger.debug(f"Received state (State up to update seq #{self.sequence})")
                break  # Done
            
                

    def request_task(self):
        # Ask for a tasks
        self.logger.debug("Asking for a task")
        if self.waiting:
            self.requester.send_multipart([b"task", f"waiting_{self.client_name}".encode()])
        else:
            self.requester.send_multipart([b"task", self.client_name.encode()])

        while True:
            try:
                key, task_b, pos_b = self.requester.recv_multipart()
            except KeyboardInterrupt:
                print("Shutdown requested... exiting")
                sys.exit(0)
            except:
                raise

            if key == b"Task":
                self.task = task_b.decode()
                self.task_g_x, self.task_g_y = unpack("!2d", pos_b)
                self.logger.debug(f"Received a task: '{self.task}' ({self.task_g_x},{self.task_g_y})")
                self.send_updates((b"task", task_b, b's'), (b"task_position", pos_b, b"!2d"), pre_encoded=True)
                self.state["client"][self.client_name]["task"] = self.task
                self.state["client"][self.client_name]["task_position"] = (self.task_g_x, self.task_g_y)
                self.busy = True
                self.progress_alarm = time.time() + self.progress_ping
                self.task_distance = dist_n1(self.g_x, self.g_y, self.task_g_x, self.task_g_y)
                self.solve_path()
                break
            elif key in [b"Wait", b"Stop"]:
                break

        return key
            

    def send_updates(self, *kv, pre_encoded=False):
        update = [self.client_name.encode()]
        if pre_encoded:
            for key, value, encoding in kv:
                update.extend([key, value, encoding])
        else:
            for key, value in kv:
                if isinstance(value, bytes):
                    update.extend([key.encode(), value, b's'])
                elif isinstance(value, str):
                    update.extend([key.encode(), value.encode(), b's'])
                elif isinstance(value, list) or isinstance(value, tuple):
                    size = len(value)
                    # We assume that all items have the same type here
                    if isinstance(value[0], float):
                        encoding = f"!{size}d"
                    elif isinstance(value[0], int):
                        encoding = f"!{size}q"
                    else:
                        self.logger.error("Update Sender: Unknow iterable value type, not numerical")
                    update.extend([key.encode(), pack(encoding, *value), encoding.encode()])
                else:
                    self.logger.error("Update Sender: Unknow value type")
        
        self.publisher.send_multipart(update)
        

    def update_state(self, subtree, client_name, kv_list):
        logging_str_list = []
        if client_name != self.client_name:

            if client_name not in self.state["client"]:
                self.logger.info(f"{client_name} is unknown, adding it to the map")
                self.state["client"][client_name] = {}

            for i in range(len(kv_list)//3):
                key_b, value_b, encoding = kv_list[3*i], kv_list[3*i+1], kv_list[3*i+2]
                key = key_b.decode()
                value = value_b.decode() if encoding == b's' else unpack(encoding, value_b)
                self.state[subtree][client_name][key] = value
                logging_str_list.append(f"{key}: {value}")

            self.logger.info(f"Server state update #{self.sequence}: {client_name}: {', '.join(logging_str_list)}")


    def run_loop(self):
        poller = zmq.Poller()
        poller.register(self.subscriber, zmq.POLLIN)

        task_alarm = 0
        progress_alarm = 0

        while True:
            if not self.busy:
                if time.time() >= task_alarm:
                    status = self.request_task()

                    if status == b"Wait":
                        self.logger.debug(f"No task available, re-checking in {self.task_ping} seconds")
                        task_alarm = time.time() + self.task_ping
                        self.waiting = True

                    elif status == b"Stop":
                        self.logger.debug(f"Received a stop signal... ")
                        break

            else:
                # If we need to update the progress
                if time.time() >= progress_alarm:
                    self.update_position()
                    distance_to_task = dist_n1(self.g_x, self.g_y, self.task_g_x, self.task_g_y)
                    if distance_to_task > self.task_distance_threshold:
                        progress = ((self.task_distance - distance_to_task) * 100) / self.task_distance
                        self.send_updates(("position", (self.g_x, self.g_y)))
                        self.logger.debug(f"Task distance = {distance_to_task:.3f} ({progress:.0f}%), position ({self.g_x:.3f}, {self.g_y:.3f}) ")
                    else:
                        self.g_x = self.task_g_x
                        self.g_y = self.task_g_y
                        self.logger.debug(f"Task '{self.task}' done!")
                        self.publisher.send_multipart([f"Task '{self.task}' done!".encode()])
                        self.progress = 0
                        self.busy = False
                    progress_alarm = time.time() + self.progress_ping
            
            try:
                items = dict(poller.poll(self.poll_timeout))
            except:
                break           # Interrupted

            if self.subscriber in items:
                
                msg = self.subscriber.recv_multipart()
                subtree, msg_sequence, *rest = msg
                msg_sequence_value = unpack("!q", msg_sequence)[0]

                if msg_sequence_value > self.sequence:
                    self.sequence = msg_sequence_value
                    state_subtree = subtree[1:-1].decode() # We need to trim the subtree b"/client/"
                    client_id_b, *kv_list = rest
                    self.update_state(state_subtree, client_id_b.decode(), kv_list)
                                    
                else:
                    self.logger.debug(f"Discarding message #{msg_sequence_value}")
        
        self.logger.debug(" Interrupted\n")
   
def main():
    client = Client()
    client.start()


if __name__ == '__main__':
    main()
