"""

   Parallelized version


"""
import sys
import argparse
from server import Server
from client import Client

from pathlib import Path
from threading import Thread
from multiprocessing import Process


def set_server(p_type):
    node, transport, context = None, None, None
    if p_type == "multithreading":
        print("Setting up server for Multi-threading...")
        transport = "inproc"
        server = Server(transport=transport)
        return server, Thread, "inproc", server.context
    elif p_type == "multiprocessing":
        print("Setting up server for Multi-processing...")
        transport = "ipc"
        server = Server(transport=transport)
        return server, Process, "ipc", None


def client_routine(context, transport):
    client = Client(context=context, transport=transport)
    client.start()


def main():

    parser = argparse.ArgumentParser(prog = 'Simulation', description = 'Agent Simulation in Parallel')

    parser.add_argument("-t", "--multi-threading", action="store_true",
                        help="Use multi-treading instead of multi-processing")
    parser.add_argument("-n", "--number-clients", type=int, default=3,
                        help="Specifies the number of clients to spawn")

    args = parser.parse_args()

    parallelization_type = "multithreading" if args.multi_threading else "multiprocessing"
    server, node, transport, context = set_server(parallelization_type)

    for _ in range(args.number_clients):
        node(target=client_routine, args=(context, transport)).start()

    server.start()
    server.close()

if __name__ == "__main__":
    main()