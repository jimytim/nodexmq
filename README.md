# NodeXMQ


Framework written in Python based on ZeroMQ for distributed node architectures. 

## Requirements
- Python 3.7+
- PyZMQ 23.2.0

## Installation in separate virtual environment

Requirement files provided for conda and venv.

Example with [Miniconda](https://docs.conda.io/en/latest/miniconda.html) installed:

```
conda env create -f env_conda.yml
conda activate zmq
```

```
conda remove --name zmq --all
```

Exemple with the native **venv** tool:

```
cd /path/to/your/env
python3 -m venv zmq
source zmq/bin/activate
python -m pip install -r env_venv.txt
```