import time
from nodexmq.lib.node import Node

def dist_n1(x0, y0, x1, y1):
    return abs(x1 - x0) + abs(y1 - y0)

def dist_n1_pair(P1, P2):
    return abs(P2[0] - P1[0]) + abs(P2[1] - P1[1])

class Bot(Node): 

    def __init__(self, setup):
        super().__init__(setup)
        self.position = self.initialize()
        self.name = f"Bot_{self.get_id()}"
        print(f"{self.name}: Spawned in {self.position}")
        self.busy = False
        self.waiting = False

        self.task_distance = None

        self.path = None
        self.task_distance_threshold = 0.2

        self.speed = 1
        self.progress = 0

        self.progress_ping = 0.5
        self.task_ping = 2
        self.task_alarm = 0
        self.progress_alarm = 0


    def input(self):
        if time.time() >= self.task_alarm:
            print(f"{self.name}: Asking for a task...")
            task_data = self.get_input()
            if task_data:
                self.task = task_data[0]
                self.task_position = task_data[1]
                self.solve_path()
                self.task_distance = dist_n1_pair(self.position, self.task_position)
                self.busy = True
                self.progress_alarm = time.time() + self.progress_ping
                print(f"{self.name}: Got task '{self.task}' in {self.task_position}")
            else:
                if self.waiting:
                    print(f"{self.name}: Still no task, shutting down.")
                    self.stop()
                else:
                    print(f"{self.name}: No task, waiting a bit...")
                    time.sleep(1)
                    self.waiting = True
                    task_alarm = time.time() + self.task_ping


    def solve_path(self):
        g_x, g_y = self.position
        t_x, t_y = self.task_position
        self.path = [[g_x, g_y],
                     [g_x, t_y],
                     [t_x, t_y]]
        self.current_segment_start_path_idx = 0
        self.current_segment = [[g_x, g_y], [g_x, t_y]]


    def update_position(self):
        """Compute bot movement
        
        Added clamping to avoid overshoot
        """
        x, y = self.position
        new_x, new_y = None, None
        updated_x, updated_y = x, y
        x0, y0 = self.current_segment[0]
        x1, y1 = self.current_segment[1]
        reach_end_segment = False
        # vertical segment (x cste)
        if x0 == x1:
            # South direction
            if y0 < y1:
                new_y = y + self.speed
                if new_y > y1:
                    updated_y = y1
                    reach_end_segment = True
                else:
                    updated_y = new_y
                    
            else: # North direction
                new_y = y - self.speed
                if new_y < y1:
                    updated_y = y1
                    reach_end_segment = True
                else:
                    updated_y = new_y
        # horizontal segment (y cste)
        elif y0 == y1:
            # East direction
            if x0 < x1:
                # updated_x = min(x + speed, x1)
                new_x = x + self.speed
                if new_x > x1:
                    updated_x = x1
                    reach_end_segment = True
                else:
                    updated_x = new_x
                
            else: # West direction
                # updated_x = max(x - speed, x1)
                new_x = x- self.speed
                if new_x < x1:
                    updated_x = x1
                    reach_end_segment = True
                else:
                    updated_x = new_x

        if reach_end_segment:
            self.current_segment_start_path_idx += 1
            idx = self.current_segment_start_path_idx
            self.current_segment = [self.path[idx], self.path[idx+1]]

        self.position = [updated_x, updated_y]


    def work(self):
        if time.time() >= self.progress_alarm:
            self.update_position()
            x, y = self.position
            distance_to_task = dist_n1_pair(self.position, self.task_position)
            if distance_to_task > 0:
                self.progress = ((self.task_distance - distance_to_task) * 100) / self.task_distance
                print(f"{self.name}: Task distance = {distance_to_task:.3f} ({self.progress:.0f}%), position ({x:.3f}, {y:.3f}) ")
            else:
                print(f"{self.name}: Task '{self.task}' done!")
                # self.task = "None"
                self.progress = 0
                self.busy = False
            self.progress_alarm = time.time() + self.progress_ping


    def run(self):
        if not self.busy:
            self.input()
        else:
            self.work()