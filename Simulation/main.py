
import sys
import argparse
import configparser

sys.path.append('../..')
from nodexmq.lib.server import Server
from nodexmq.lib.helpers import generate_tasks
from bot import Bot

from pathlib import Path
from multiprocessing import Process

import json


class Setup:

    def __init__(self, 
                config_file_path,
                client_schema_file_path,
                transport = "ipc"):
        self.config = configparser.ConfigParser()
        self.config.read(config_file_path)
        self.transport = transport or self.config["Server"]["Transport"]
        self.full_schema = None
        self.client_schema = None
        self.tracked_variables = None
        self.encoding = None
        self.inputs = None
        self.load_schema(client_schema_file_path)
        self.load_inputs()
        

    def load_schema(self, file_path):
        with open(file_path) as f:
            self.full_schema = json.load(f)
            self.client_schema = self.full_schema["properties"]
            self.tracked_variables = list(self.client_schema.keys())
            self.encoding = {key:self.client_schema[key]["encoding"] for key in self.client_schema.keys()}

    def load_inputs(self):
        input_parameters = self.config["Input Parameters"]
        n_rows = int(input_parameters["NbRows"])
        n_cols = int(input_parameters["NbColumns"])
        n_tasks = int(input_parameters["NbTasks"])
        random_seed = int(input_parameters["RandomSeed"])
        self.inputs = generate_tasks(n_rows, n_cols, n_tasks, random_seed=random_seed)


def node_routine(setup):
    bot = Bot(setup)
    bot.execute()


def main():

    parser = argparse.ArgumentParser(prog = 'Simulation', description = 'Agent Simulation in Parallel')

    parser.add_argument("-n", "--number-nodes", type=int, default=3,
                        help="Specifies the number of clients to spawn")

    args = parser.parse_args()
  
    setup = Setup("../Config/config.txt",
                  "../Config/client_state_schema.json")

    server = Server(setup=setup)
    for _ in range(args.number_nodes):
        Process(target=node_routine, args=(setup,)).start()

    server.start()
    server.close()

if __name__ == "__main__":
    main()